import { async, ComponentFixture, TestBed, inject } from '@angular/core/testing';
import { HttpClientModule } from '@angular/common/http';
import { ExternoService } from 'src/app/services/externo.service';


import { TablaComponent } from './tabla.component';

describe('TablaComponent', () => {
  let component: TablaComponent;
  let fixture: ComponentFixture<TablaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ExternoService],
      declarations: [TablaComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TablaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  afterEach(() => {
    component.ngOnDestroy();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be created', inject([ExternoService], (service: ExternoService) => {
    expect(service).toBeTruthy();
  }));

  it('should get json', async(() => {
    const service: ExternoService = TestBed.get(ExternoService);
    service.getService('sale.json').subscribe(
      (response) => expect(response).not.toBeNull(),
      (error) => fail(error)
    );
  }));


  it('should return an array with data', () => {
    expect(component.LoadData).not.toBeNull();
  });


  it('should return months', () => {
    component.ngOnInit();
    expect(component.meses).not.toBeNull();
  });

  it('should return grouped array', () => {
    let mes =  [{
        "mes": "enero",
        "id": 1,
        "total": 849
      }];
    component.ngOnInit();
    expect(component.groupBy(mes, 'mes')).not.toBeNull();

  });

});
