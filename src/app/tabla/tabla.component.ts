import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
/** importamos el servicio para consumir el json */
import { ExternoService } from 'src/app/services/externo.service';


@Component({
  selector: 'app-tabla',
  templateUrl: './tabla.component.html',
  styleUrls: ['./tabla.component.css']
})
export class TablaComponent implements OnInit {
  meses: any;
  total: number;
  fecha: Subscription;

  constructor(private externoService: ExternoService) { }

  ngOnInit() {
    this.LoadData();
  }

  /**
 * @description Obtenemos los datos de el JSON
 * @returns los datos en un array
 * @author Sandra Gil Rosales
 */
  LoadData() {
    const opciones = { month: 'long' };
    this.fecha = this.externoService.getService('sale.json').subscribe((res: any) => {
      if (res !== '') {
        this.total = 0
        res.data.forEach(element => {
          this.total += element.total;
          let mes = new Date(element.date);
          element.mes = mes.toLocaleDateString("es-ES", opciones);
        });
        this.meses = this.groupBy(res.data, 'mes');
      } else {
        console.log('no trae datos');
      }
    });

  }
  /**
* @description funcion para agrupar dependiendo de el mes
* @returns los datos en un array
* @author Sandra Gil Rosales
*/
  groupBy(arr: any, agrupador: string) {
    const resultado = arr.reduce((fechas, item) => {
      var val = item[agrupador];
      fechas[val] = fechas[val] || { mes: item.mes, total: 0 };
      fechas[val].total += item.total;
      return fechas;
    }, {});
    const arrResult = Object.values(resultado);
    return arrResult;
  }

  ngOnDestroy() {
    this.fecha.unsubscribe();
  }
}
